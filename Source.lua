local function class() 
    local Class = {};
    Class.__index = Class;

    function Class.new(...) 
        local classInst = setmetatable({}, Class);

        classInst:__init(...);
        
        return classInst;
    end;

    return Class;
end;

local hashTable = class(); do 
    local byte = string.byte;

    function hashTable:__init(size) 
        self.size = size;
        self.array = table.create(size, nil);
    end;

    function hashTable:calucateHash(key) 
        local hash = 0;

        for i = 1, #key do 
            hash = hash + byte(key, i);
        end;
        
        return hash % self.size;
    end;

    function hashTable:get(key) 
        local value = self.array[self:calucateHash(key)];

        return value;
    end;

    function hashTable:set(key, value) 
        self.array[self:calucateHash(key)] = value;
    end;
end;